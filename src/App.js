import React,{Component} from 'react';
import logo from './logo.svg';
import './App.css';
import uuid from 'react-uuid';
import moment from "moment";
import { Layout, Menu, Breadcrumb,Card } from 'antd';
import ReactPlayer from 'react-player'
import AWS from 'aws-sdk';
import awsmobile from './aws-exports';
import axios from 'axios';
const { Header, Content, Footer } = Layout;
AWS.config.update({
  region: awsmobile.aws_cognito_region,
  credentials: new AWS.CognitoIdentityCredentials({
    IdentityPoolId: awsmobile.aws_cognito_identity_pool_id
  })
});
class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isAuthenticated: false,
      isAuthenticating: true,
      username:'',
      Group:'',
      Userinfo:'',
      email: '',
      password: '',
      start:false,
      end:false,
      src:'',
      data:{},
      id:''	,
      videoplayed:false,
      status:{},
      enable:true,
      logoimg:'',
      cname:'',
      cntrl:true,
      ptoken:'',
      poster:''
		};
  }

  async componentDidMount() {
    console.log("adata",window.init_data);
    console.log("ddata",window.page_type);
    window.addEventListener("resize", this.resize.bind(this));
    this.resize();
    var id='';//const { params1 } = this.props.match
    if(window.page_type)
    {
      id=window.page_type;
      this.setState({id})
    }
    else{
      id=this.props.match.params.id
      this.setState({id})
    }
    console.log("params",this.props.match.params.id);
   /* var kinesis = new AWS.Kinesis({region : 'us-east-1'});
    var currTime = new Date().getMilliseconds();
    var sensor = 'sensor-' + Math.floor(Math.random() * 100000);
    var reading = Math.floor(Math.random() * 1000000);

    var record = JSON.stringify({
      time : currTime,
      sensor : sensor,
      reading : reading
    });

    var recordParams = {
      Data : record,
      PartitionKey : sensor,
      StreamName : 'NEWCSTEST'
    };

    kinesis.putRecord(recordParams, function(err, data) {
      if (err) {
        console.log(err);
      }
      else {
        console.log('Successfully sent data to Kinesis.');
      }
    });*/
  
    try
    {
   
    var res=await axios.get("https://eo7wedpkj4.execute-api.us-west-2.amazonaws.com/test/api/v1/enduser/"+id);
    console.log("res",res.data);
    var x=[]
    x.push(res.data);
    this.setState({BatchId:res.data.BatchId})
   
this.setState({data:res.data.Item})
this.setState({poster:"https://wardlaw-claims-prod-images.s3.amazonaws.com/WDGL_Imagelet_"+this.state.data.SubscriptionType+".png"})
 

  try{
     var res2 = await axios.get(`https://eo7wedpkj4.execute-api.us-west-2.amazonaws.com/test/api/v1/company/getlogo/${res.data.Item.Company}`);
this.setState({ logoimg: "data:image/*;base64," + res2.data.base64 });
}
catch(e)
{
}
var params = {
  "TransactionId": id,
  "timezone":moment(new Date(), ["HH:mm A"]).format('HH')
}
try{
//var res=await axios.post(`https://eo7wedpkj4.execute-api.us-west-2.amazonaws.com/test/api/v1/sundaysky/SessionToken`,params)
var res=await axios.post(`https://eo7wedpkj4.execute-api.us-west-2.amazonaws.com/test/api/v1/sundaysky/SessionToken`,params)
//var res=await axios.post(`http://localhost:3001/api/v1/sundaysky/SessionToken`,params)

this.setState({src:res.data});
this.setState({ptoken:res.data.playerToken})
//console.log(this.src);
//alert(this.state.ptoken)
}
catch(err)
{
//console.log(err);
}

/*
var params='';
if(res.data.Item.SubscriptionType==="RENEWALS")
{
  params = {
    "repositories": {
      "hsrc": "https://api.sundaysky.com/resolver/baseurls/0013300001leb6SAAQ/ward-claims-demo/dev"
  },
  "story": {
      "hsrc": "Renewal",
      "repository": "story",
      "settings": {
          "output": {
              "formats": {
                  "video": {
                      "format": "m3u8"
                  }
              }
          }
      }
  },

    "data": {
        "dataElements": {
          /*  "CTABgColor":this.state.data.PrimaryColor1,
            "CTAText": "Click",
            "CTAUrl": "https://www.google.com",
            "Data1": this.state.data.Email,
            "Data2": this.state.data.Phone,
            "Data3": this.state.data.SubscriptionType,
            "FirstName": this.state.data.Name,
            "FirstNameColor": this.state.data.PrimaryColor2,
            "FirstNameUrl": "https://cdn.psv.one/assets/Hello_David.mp3",
            "LogoUrl": "https://cdn.psv.one/assets/WardlawDigital/WARD_Digital_Logo_Lockup_Color_Logo.png"*/

          /*  "Primary1":res.data.Item.PrimaryColor1,
            "Primary2":res.data.Item.PrimaryColor2,
            "Accent1":res.data.Item.SecondaryColor1,
            "Accent2":res.data.Item.SecondaryColor2,
            "localTime24Hour": moment(new Date(), ["HH:mm A"]).format('HH'),
            "logo":  this.state.logoimg,
            "firstName": res.data.Item.Name,
            "MortgageLender":res.data.Item.FieldList.mlender,
            "PolicyAge": res.data.Item.FieldList.policyage,
            "PolicyRenewStart":moment(res.data.Item.FieldList.rdate).format('DD-MM-YYYY'),
            "PolicyRenewEnd":moment(res.data.Item.FieldList.edate).format('DD-MM-YYYY'),
            "PropertyCityState":res.data.Item.FieldList.add2?res.data.Item.FieldList.add1+","+res.data.Item.FieldList.add2:res.data.Item.FieldList.add1,
            "PropertyStreet": res.data.Item.FieldList.pstreet,
            "ShowFloodInsuranceScene": res.data.Item.FieldList.sfinsur, // Default false
            "ShowLawOrdinanceScene": res.data.Item.FieldList.slinsur, // Default false
            "ShowPortalScene": res.data.Item.FieldList.spinsur // Default false
        }
    }
}

}
else
{
    params = {
      "repositories": {
          "hsrc": "https://api.sundaysky.com/resolver/baseurls/0013300001leb6SAAQ/test-program/dev"
         //"hsrc": "https://api.sundaysky.com/resolver/baseurls/0013300001leb6SAAQ/ward-claims-demo/dev"
               
      },
      "story": {
          "hsrc": "WardlawDigital",
          "repository": "story",
          "settings": {
              "output": {
                  "formats": {
                      "video": {
                          "format": "m3u8"
                      }
                  }
              }
          }
      },
      "data": {
          "dataElements": {
            "CTABgColor":this.state.data.PrimaryColor1,
            "CTAText": "Click",
            "CTAUrl": "https://www.google.com",
             "Data1": this.state.data.ChannelType=='EMAIL'?this.state.data.Email:this.state.data.Phone,
           // "Data2": this.state.data.this.state.data.Phone,
            "Data3": this.state.data.SubscriptionType,
            "FirstName": this.state.data.Name,
            "FirstNameColor": this.state.data.PrimaryColor2,
            "FirstNameUrl": "https://cdn.psv.one/assets/Hello_David.mp3",
            "LogoUrl": "https://cdn.psv.one/assets/WardlawDigital/WARD_Digital_Logo_Lockup_Color_Logo.png"
          }
      }
  }
}


}
catch(e)
{

}
  console.log("111data",params)
  try{
  
  var res=await axios.post('https://api.sundaysky.com/vlx/video',JSON.stringify(params),{
    headers: {
      'x-api-key': 'SF-00000380:dev:c2303d75376a2e76655301a96f5283b2',
     
      'Content-Type': 'application/json',
     
  }
   });
  
  this.setState({src:res.data._links['video-stream'].href});
  console.log("res",res.data._links['video-stream'].href);

*/
  
  }
  catch(err)
  {
    console.log(err);
  }



  }
  handleProgress =async state => {
    if(this.state.start)
    {
    console.log('onProgress', JSON.stringify(state));
    var kinesis = new AWS.Kinesis({region : 'us-east-1'});
    var currTime = new Date().getMilliseconds();
    var sensor =  this.state.id;
    var reading = uuid();

    var record = JSON.stringify({
      info:state,
      "TransactionId":sensor,
      "videoId":reading,
      "landingpageloaded":true,
      "videoplayed":this.state.videoplayed,
      "CTAClicked":false,
      "CTADisplayed":false,
      "BatchId":this.state.data.BatchId
    });
//console.log("rec",record)
    var recordParams = {
      Data : record,
      PartitionKey : reading,
      StreamName : 'wardlaw'
    };
    console.log("rec",recordParams)
    kinesis.putRecord(recordParams, function(err, data) {
      if (err) {
        console.log(err);
      }
      else {
        console.log('Successfully sent data to Kinesis.');
      }
    });


    }
    if(state.played===1)
    {
      this.setState({status:state})
      this.setState({end:true});
    }
  }
  test=async event=>{
    var myPlayer = document.getElementById('sskyplayer');
    var id= this.state.id;
    var BatchId=this.state.BatchId;
    myPlayer.addEventListener('progress', function(event) {


     //console.log('onProgress', JSON.stringify(state));
      var kinesis = new AWS.Kinesis({region : 'us-east-1'});
      var currTime = new Date().getMilliseconds();
      var sensor =  id;
      var reading = uuid();
  
      var record = JSON.stringify({
        info:event.detail.position,
        "TransactionId":sensor,
        "videoId":reading,
        "landingpageloaded":true,
        "videoplayed":true,
        "CTAClicked":false,
        "CTADisplayed":false,
        "BatchId":BatchId
      });
  //console.log("rec",record)
      var recordParams = {
        Data : record,
        PartitionKey : reading,
        StreamName : 'wardlaw'
      };
      console.log("rec",recordParams)
      kinesis.putRecord(recordParams, function(err, data) {
        if (err) {
          console.log(err);
        }
        else {
          console.log('Successfully sent data to Kinesis.');
        }
      });
   //   alert("eeee1"+event.detail.position);
    if(event.detail.position=='0.95') {
    // Do Something
    
    }
    });
    myPlayer.addEventListener('ctaClicked', function(event) {
    console.log("xxx",event.detail)
    });
  }
  handlePlay=async event=>{
    this.setState({start:true});
    this.setState({videoplayed:true})
  }
  handleckick=event=>{
    if(this.state.end)
    {
   //   alert("CTA Clicked");
      var kinesis = new AWS.Kinesis({region : 'us-east-1'});
    var currTime = new Date().getMilliseconds();
    var sensor =  this.state.id;
    var reading = uuid();

    var record = JSON.stringify({
      info:this.state.status,
      "TransactionId":sensor,
      "videoId":reading,
      "landingpageloaded":true,
      "videoplayed":this.state.videoplayed,
      "CTAClicked":true,
      'CTADisplayed':true,
      "BatchId":this.state.data.BatchId
    });
//console.log("rec",record)
this.setState({enable:false})
    var recordParams = {
      Data : record,
      PartitionKey : reading,
      StreamName : 'wardlaw'
    };
    console.log("rec",recordParams)
    kinesis.putRecord(recordParams, function(err, data) {
      if (err) {
        console.log(err);
      }
      else {
        
        console.log('Successfully sent data to Kinesis.');
      }
    });
   
    }
  }
  resize() {
    let currentHideNav = (window.innerWidth <= 760);
    if (currentHideNav !== this.state.hideNav) {
        this.setState({hideNav: currentHideNav});
        console.log('1resize',this.state.hideNav);
    }
}
  render() {
    console.log("adata",window);
  return (
    <Layout>
   <Header style={{ position: 'fixed', zIndex: 1, width: '100%',    boxShadow: "2px 5px 9px #888887" }}>
  <div className="logo" >{this.state.logoimg?<img src={this.state.logoimg}style={{height: "50px", width: "auto"}}/>:<h2>{this.state.data.CompanyName}</h2>}</div>
    
    </Header>
    <Content className="site-layout" style={{
    marginLeft: "1px",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover"}}><br/>
     
    {/* <div className="site-layout-background" style={{ padding: 24 }}>*/}
     {/* <Card style={{ width: "785px",boxShadow: "14px 12px 9px #888887",marginLeft: "116px"}} >*/}
      <div className="player-wrapper" style={{ 
  margin: "auto",
width: "70%",
height: "auto",
/*paddingLeft: this.state.src?"184%":"67%",
paddingRight: "73%",
marginLeft: "-78%",*/
paddingTop: this.state.hideNav? "60px" : "4%"}}>{/*{this.state.src?
      <ReactPlayer
              ref={this.ref}
              className='react-player'
             
              url={this.state.src.data}
              playsinline
              controls={this.state.cntrl}
            
              onProgress={this.handleProgress}
              onPlay={this.handlePlay}
              onClick={this.handleckick}
              width='100%'
              height='89%'
              style={{marginTop:"54px"}}
      />*/}
      {this.state.ptoken!=''?
  <sundaysky-video id="sskyplayer" session={this.state.ptoken}
  poster={this.state.poster} lang="en"  className='react-player' style={{position: "relative",width:"100%"}}     
  onClick={this.test}></sundaysky-video>
  
      :<div style={{marginLeft: this.state.hideNav?"1%":"45%",
        /* margin-bottom: -4px; */
        marginTop: "-25%"}}><img src={require("./220.gif") }/><p style={{paddingLeft: "62px"}}>Loading...</p></div>}</div>
  {/*</Card>
  
     
  </div>*/}


 </Content>
 {/*  <Footer style={{ textAlign: 'center' }}>© 2020 All Rights Reserved. Powered by <a href="https://www.wardlawdigital.com" target="_blank">Wardlaw Digital</a></Footer>
  */}</Layout>
  );
}
}

export default App;
